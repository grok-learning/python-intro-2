cars = {}

line = input('Car: ')
while line:
  cars[line] = cars.get(line, 0) + 1
  line = input('Car: ')

for colour in cars:
  print('Cars that are ' + colour + ':', cars[colour])
