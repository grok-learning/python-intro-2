file = open('input.txt').read().lower() 
listed = file.split('\n')  
for index, sentence in enumerate(listed):
  a = sentence.count('a')
  r = sentence.count('r')
  d = sentence.count('d')
  v = sentence.count('v')
  k = sentence.count('k')
  if a>=3 and r>=2 and d>=1 and v>=1 and k>=1:
    print('Aardvark on line '+str(index+1))
