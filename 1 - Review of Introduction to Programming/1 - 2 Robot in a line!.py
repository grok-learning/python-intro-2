line = input("Line: ")

if "robot " in line:
  print("There is a small robot in the line.")
elif "ROBOT " in line:
  print("There is a big robot in the line.")
elif 'robot ' in line.lower():
  print("There is a medium sized robot in the line.")
else:
  print("No robots here.")
